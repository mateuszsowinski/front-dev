import ENGLISH_LANG from './EnglishLang';
import POLISH_LANG from './PolishLang';

export default { en: ENGLISH_LANG, pl: POLISH_LANG };

import {
  GET_PRODUCT,
  GET_PRODUCTS,
  EDIT_PRODUCT_ITEM,
  EDIT_FOLLOW_ITEM,
  GET_PRODUCT_ALERT_CONFIRM,
  GET_PRODUCT_ALERT_WARNING,
  GET_PRODUCT_ALERT_WRONG
} from './pages/HomePage/HomePage.query';
import faker from 'faker';

export const Mocks = [
  {
    request: {
      query: GET_PRODUCT
    },
    result: {
      data: {
        product: {
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: GET_PRODUCTS
    },
    result: {
      data: {
        products: [
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: '-15%'
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: '-15%'
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: null
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: null
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: '-15%'
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: '-15%'
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: null
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: null
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: '-15%'
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: '-15%'
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: null
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: null
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: '-15%'
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: faker.random.number()
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: null
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: null
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: '-15%'
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: '-15%'
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: null
          },
          {
            id: faker.random.number(),
            salePrice: faker.commerce.price(),
            startingPrice: faker.commerce.price(),
            name: faker.name.findName(),
            subname: faker.company.companyName(),
            title: faker.company.companyName(),
            subtitle: faker.random.word(),
            more: faker.lorem.text(),
            discount: null
          }
        ]
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: true
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_PRODUCT_ITEM,
      variables: {
        isAddProduct: false
      }
    },
    result: {
      data: {
        editAddProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: false
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: false,
          isFollowProduct: false,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: EDIT_FOLLOW_ITEM,
      variables: {
        isFollowProduct: true
      }
    },
    result: {
      data: {
        editFollowProductItem: {
          isAddProduct: true,
          isFollowProduct: true,
          id: faker.random.number(),
          salePrice: faker.commerce.price(),
          startingPrice: faker.commerce.price(),
          name: faker.name.findName(),
          subname: faker.company.companyName(),
          title: faker.company.companyName(),
          subtitle: faker.random.word(),
          more: faker.lorem.text(),
          discount: '-15%'
        }
      }
    }
  },
  {
    request: {
      query: GET_PRODUCT_ALERT_CONFIRM
    },
    result: {
      data: {
        productAlertConfirm: {
          id: faker.random.number(),
          productInformation: 'Produkt dopasowany do wybranego samochodu:',
          productName: 'MERCEDES S500 (8E2, B6) 1.8 T quattro, 140 PS, 103kW'
        }
      }
    }
  },
  {
    request: {
      query: GET_PRODUCT_ALERT_WARNING
    },
    result: {
      data: {
        productAlertWarning: {
          id: faker.random.number(),
          title: 'Zanim kupisz!',
          subtitle: 'Sprawdź, czy produkt pasuje do Twojego samochodu',
          description: 'Nasz sklep umożliwia ściśle dopasowanie produktów do wybranego typu pojazdu. Zanim kupisz produkt w sklepie upewnij się czy pasuje do Twojego auta. Wystarczy...'
        }
      }
    }
  },
  {
    request: {
      query: GET_PRODUCT_ALERT_WRONG
    },
    result: {
      data: {
        productAlertWrong: {
          id: faker.random.number(),
          productInformation: 'Część nie pasuje do wybranego pojazdu:',
          productName: 'MERCEDES S500 (8E2, B6) 1.8 T quattro, 140 PS, 103kW'
        }
      }
    }
  }
];

export default Mocks;

import styled from 'styled-components';
import { Panel as commonPanel } from 'assets/Common/SignPanel.style';

export const Panel = styled.div`
  ${commonPanel}
`;

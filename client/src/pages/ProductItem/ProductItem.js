import React from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

const GET_PRODUCT_ITEM = gql`
  {
    productItem {
      SalePrice
      StartingPrice
      name
      subname
      Title
      Subtitle
      Discount
    }
  }
`;

const ProductItem = ({ onProductItemSelected }) => (
  <Query query={GET_PRODUCT_ITEM}>
    {({ loading, error, data }) => {
      if (loading) return 'Loading...';
      if (error) return `Error! ${error.message}`;

      return (
        <select name="productItem" onChange={onProductItemSelected}>
          {data.productItem.map(productItem => (
            <option key={productItem.name} value={productItem.subname}>
              {productItem.Subname}
            </option>
          ))}
        </select>
      );
    }}
  </Query>
);

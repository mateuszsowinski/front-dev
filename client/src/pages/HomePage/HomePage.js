import React from 'react';
import { Query } from 'react-apollo';
import {
  GET_PRODUCT,
  GET_PRODUCTS,
  GET_PRODUCT_ALERT_CONFIRM,
  GET_PRODUCT_ALERT_WARNING,
  GET_PRODUCT_ALERT_WRONG
} from './HomePage.query';

import Helmet from 'components/Helmet';
import ProductItem from 'components/ProductItem';
import ProductList from 'components/ProductList/ProductList';

import ProductAlertConfirm from 'components/ProductAlertConfirm/ProductAlertConfirm';
import ProductAlertWarning from 'components/ProductAlertWarning/ProductAlertWarning';
import ProductAlertWrong from 'components/ProductAlertWrong/ProductAlertWrong';

import {
  ListProduct,
  Array,
  SingleItem,
  Alerts
} from '../../components/ProductItem/ProductItem.style';

export const HomePage = () => {
  return (
    <>
      <Helmet title={'LANG_PAGE_TITLE.HOME'} message={'Home'} />
      <Array>Array</Array>
      <ListProduct>
        <Query query={GET_PRODUCTS}>
          {({ loading, error, data }) => {
            if (loading) return <div>Loading...</div>;
            if (error) return <div>Error! {error.message}</div>;
            return <ProductList products={data.products} />;
          }}
        </Query>
      </ListProduct>
      <SingleItem>Single item</SingleItem>
      <Query query={GET_PRODUCT}>
        {({ loading, error, data }) => {
          if (loading) return <div>Loading...</div>;
          if (error) return <div>Error! {error.message}</div>;
          return <ProductItem data={data.product} />;
        }}
      </Query>
      <Alerts>Alerts</Alerts>
      <Query query={GET_PRODUCT_ALERT_CONFIRM}>
        {({ loading, error, data }) => {
          if (loading) return <div>Loading...</div>;
          if (error) return <div>Error! {error.message}</div>;
          return <ProductAlertConfirm data={data.productAlertConfirm} />;
        }}
      </Query>
      <Query query={GET_PRODUCT_ALERT_WARNING}>
        {({ loading, error, data }) => {
          if (loading) return <div>Loading...</div>;
          if (error) return <div>Error! {error.message}</div>;
          return <ProductAlertWarning data={data.productAlertWarning} />;
        }}
      </Query>
      <Query query={GET_PRODUCT_ALERT_WRONG}>
        {({ loading, error, data }) => {
          if (loading) return <div>Loading...</div>;
          if (error) return <div>Error! {error.message}</div>;
          return <ProductAlertWrong data={data.productAlertWrong} />;
        }}
      </Query>
    </>
  );
};

export default HomePage;

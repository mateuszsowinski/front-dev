import gql from 'graphql-tag';

export const GET_PRODUCT = gql`
  {
    product {
      id
      salePrice
      startingPrice
      name
      subname
      title
      subtitle
      more
      discount
    }
  }
`;

export const GET_PRODUCTS = gql`
  {
    products {
      id
      salePrice
      startingPrice
      name
      subname
      title
      subtitle
      more
      discount
    }
  }
`;

export const GET_PRODUCT_ALERT_CONFIRM = gql`
  {
    productAlertConfirm {
      id
      productInformation
      productName
    }
  }
`;

export const GET_PRODUCT_ALERT_WARNING = gql`
  {
    productAlertWarning {
      id
      title
      subtitle
      description
    }
  }
`;

export const GET_PRODUCT_ALERT_WRONG = gql`
  {
    productAlertWrong {
      id
      productInformation
      productName
    }
  }
`;

export const EDIT_PRODUCT_ITEM = gql`
  mutation editAddProductItem($isAddProduct: Boolean!) {
    editAddProductItem(isAddProduct: $isAddProduct) {
      isAddProduct
      isFollowProduct
      id
      salePrice
      startingPrice
      name
      subname
      title
      subtitle
      more
      discount
    }
  }
`;

export const EDIT_FOLLOW_ITEM = gql`
  mutation editFollowProductItem($isFollowProduct: Boolean!) {
    editFollowProductItem(isFollowProduct: $isFollowProduct) {
      isAddProduct
      isFollowProduct
      id
      salePrice
      startingPrice
      name
      subname
      title
      subtitle
      more
      discount
    }
  }
`;

import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';

import { shallow, mount } from 'enzyme';
import ProductAlertWrong from './ProductAlertWrong';

import Mocks from '../../Mocks';

describe('Product Alert Wrong', () => {
  it('Init', () => {
    const init = shallow(<ProductAlertWrong />);
    expect(init);
  });
  it('With all data', () => {
    const withData = shallow(
      <ProductAlertWrong
        data={{
          ProductInformation: 'Część nie pasuje do wybranego pojazdu:',
          ProductName: 'MERCEDES S500 (8E2, B6) 1.8 T quattro, 140 PS, 103kW'
        }}
      />
    );
    expect(withData);
  });
  it('With all data without product information', () => {
    const withoutProductInformation = shallow(
      <ProductAlertWrong
        data={{
          ProductInformation: null,
          ProductName: 'MERCEDES S500 (8E2, B6) 1.8 T quattro, 140 PS, 103kW'
        }}
      />
    );
    expect(withoutProductInformation);
  });
  it('With all data without product name', () => {
    const withoutProductName = shallow(
      <ProductAlertWrong
        data={{
          ProductInformation: 'Część nie pasuje do wybranego pojazdu:',
          ProductName: null
        }}
      />
    );
    expect(withoutProductName);
  });
  it('Mock with all data', () => {
    const mockTest = mount(
      <MockedProvider mocks={Mocks} addTypename={false}>
        <ProductAlertWrong />
      </MockedProvider>
    );
    expect(mockTest);
  });
});

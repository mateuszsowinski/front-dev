import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import { storiesOf, addDecorator } from '@storybook/react';
import { IntlProviderWrapper } from '../../IntlContext';
import CssBaseline from '@material-ui/core/CssBaseline';

import ProductAlertWrong from './ProductAlertWrong';
import Mocks from '../../Mocks';

addDecorator(story => (
  <CssBaseline>
    <MockedProvider mocks={Mocks} addTypename={false}>
      <IntlProviderWrapper>{story()}</IntlProviderWrapper>
    </MockedProvider>
  </CssBaseline>
));

storiesOf('Product Alert Wrong', module)
  .add('Init', () => <ProductAlertWrong data={{}} />)
  .add('With all data', () => (
    <ProductAlertWrong
      data={{
        ProductInformation: 'Część nie pasuje do wybranego pojazdu:',
        ProductName: 'MERCEDES S500 (8E2, B6) 1.8 T quattro, 140 PS, 103kW'
      }}
    />
  ))
  .add('With all data without product information', () => (
    <ProductAlertWrong
      data={{
        ProductInformation: null,
        ProductName: 'MERCEDES S500 (8E2, B6) 1.8 T quattro, 140 PS, 103kW'
      }}
    />
  ))
  .add('With all data without product name', () => (
    <ProductAlertWrong
      data={{
        productInformation: 'Część nie pasuje do wybranego pojazdu:',
        productName: null
      }}
    />
  ))
  .add('Mock with all data', () => (
    <MockedProvider mocks={Mocks} addTypename={false}>
      <ProductAlertWrong />
    </MockedProvider>
  ));

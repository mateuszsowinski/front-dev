import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';

import { shallow, mount } from 'enzyme';
import ProductAlertConfirm from './ProductAlertConfirm';

import Mocks from '../../Mocks';

describe('Product Alert Confirm', () => {
  it('Init', () => {
    const init = shallow(<ProductAlertConfirm />);
    expect(init);
  });
  it('With all data', () => {
    const withData = shallow(
      <ProductAlertConfirm
        data={{
          ProductInformation: 'Produkt dopasowany do wybranego samochodu:',
          ProductName: 'MERCEDES S500 (8E2, B6) 1.8 T quattro, 140 PS, 103kW'
        }}
      />
    );
    expect(withData);
  });
  it('With all data without product information', () => {
    const withoutProductInformation = shallow(
      <ProductAlertConfirm
        data={{
          ProductInformation: null,
          ProductName: 'MERCEDES S500 (8E2, B6) 1.8 T quattro, 140 PS, 103kW'
        }}
      />
    );
    expect(withoutProductInformation);
  });
  it('With all data without product name', () => {
    const withoutProductName = shallow(
      <ProductAlertConfirm
        data={{
          ProductInformation: 'Produkt dopasowany do wybranego samochodu:',
          ProductName: null
        }}
      />
    );
    expect(withoutProductName);
  });
  it('Mock with all data', () => {
    const mockTest = mount(
      <MockedProvider mocks={Mocks} addTypename={false}>
        <ProductAlertConfirm />
      </MockedProvider>
    );
    expect(mockTest);
  });
});

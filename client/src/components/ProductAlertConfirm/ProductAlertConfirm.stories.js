import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import { storiesOf, addDecorator } from '@storybook/react';
import { IntlProviderWrapper } from '../../IntlContext';
import CssBaseline from '@material-ui/core/CssBaseline';

import ProductAlertConfirm from './ProductAlertConfirm';
import Mocks from '../../Mocks';

addDecorator(story => (
  <CssBaseline>
    <MockedProvider mocks={Mocks} addTypename={false}>
      <IntlProviderWrapper>{story()}</IntlProviderWrapper>
    </MockedProvider>
  </CssBaseline>
));

storiesOf('Product Alert Confirm', module)
  .add('Init', () => <ProductAlertConfirm data={{}} />)
  .add('With all data', () => (
    <ProductAlertConfirm
      data={{
        ProductInformation: 'Produkt dopasowany do wybranego samochodu:',
        ProductName: 'MERCEDES S500 (8E2, B6) 1.8 T quattro, 140 PS, 103kW'
      }}
    />
  ))
  .add('With all data without product information', () => (
    <ProductAlertConfirm
      data={{
        ProductInformation: null,
        ProductName: 'MERCEDES S500 (8E2, B6) 1.8 T quattro, 140 PS, 103kW'
      }}
    />
  ))
  .add('With all data without product name', () => (
    <ProductAlertConfirm
      data={{
        ProductInformation: 'Produkt dopasowany do wybranego samochodu:',
        ProductName: null
      }}
    />
  ))
  .add('Mock with all data', () => (
    <MockedProvider mocks={Mocks} addTypename={false}>
      <ProductAlertConfirm />
    </MockedProvider>
  ));

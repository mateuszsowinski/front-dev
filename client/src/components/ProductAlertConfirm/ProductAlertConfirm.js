import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Query } from 'react-apollo';
import { GET_PRODUCT_ALERT_CONFIRM } from '../../pages/HomePage/HomePage.query';
import {
  Box,
  ProductInformation,
  ProductName,
  ButtonEdit
} from './ProductAlertConfirm.style';

import { FaCheckSquare, FaTimes } from 'react-icons/fa';

import { IconContext } from 'react-icons';

function ProductAlertConfirm({ data }) {
  return (
    <>
      <Query query={GET_PRODUCT_ALERT_CONFIRM}>
        {({ loading, error, data }) => {
          console.log(loading, error, data);
          const { productAlertConfirm } = data;
          if (loading) return <Box>Loading...</Box>;
          if (error) return <Box>Error! {error.message}</Box>;
          return (
            <Box>
              <IconContext.Provider
                value={{ style: { height: '30px', width: '30px' } }}
              >
                <>
                  <FaCheckSquare />
                </>
              </IconContext.Provider>
              <IconContext.Provider
                value={{
                  style: {
                    margin: '-17px 0px 0px 320px',
                    height: '15px',
                    width: '15px',
                    fontWeight: 'bold'
                  }
                }}
              >
                <>
                  <FaTimes />
                </>
              </IconContext.Provider>
              <ProductInformation>
                {productAlertConfirm.productInformation}
              </ProductInformation>
              <ProductName>{productAlertConfirm.productName}</ProductName>
              <ButtonEdit>
                <FormattedMessage
                  id={'LANG_PRODUCT_ALERT_CONFIRM_&_WRONG.BUTTON_EDIT'}
                  defaultMessage={'Change'}
                />
              </ButtonEdit>
            </Box>
          );
        }}
      </Query>
    </>
  );
}

export default ProductAlertConfirm;

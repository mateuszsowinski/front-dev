import styled from 'styled-components';
import { Colors } from 'assets/Variables.style';

export const Box = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: 40px 300px;
  grid-template-rows: 13px 30px 40px;
  height: 87px;
  width: 350px;
  padding: 5px;
  margin-bottom: 20px;
  background-color: ${Colors.mercuryTwo};
  padding: 10px;
`;

export const ProductInformation = styled.div`
  position: relative;
  grid-column: 2/2;
  grid-row: 1/3;
  font-size: 11px;
`;

export const ProductName = styled.div`
  position: relative;
  grid-column: 2/2;
  grid-row: 2/3;
  font-size: 12px;
  font-weight: bold;
`;

export const ButtonEdit = styled.button`
  position: relative;
  display: inline-block;
  grid-column: 2/2;
  grid-row: 3/3;
  height: 20px;
  width: 45px;
  padding: 5px 5px 5px 5px;
  margin-top: 3px;
  margin-left: 230px;
  border: 0;
  border-radius: 3px 3px 3px 3px;
  color: ${Colors.white};
  background-color: ${Colors.gray};
  text-decoration: none;
  font-size: 11px;
  text-align: center;
`;

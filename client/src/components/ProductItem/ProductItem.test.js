import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';

import { shallow, mount } from 'enzyme';
import ProductItem from './ProductItem';

import Mocks from '../../Mocks';

describe('Product Item', () => {
  it('Init', () => {
    const init = shallow(<ProductItem />);
    expect(init);
  });
  it('With all data', () => {
    const withData = shallow(
      <ProductItem
        data={{
          name: 'Havoline Magnetic',
          subname: 'EVO 15W-40',
          title: 'Olej przekładniowy',
          salePrice: '149,00 zł',
          startingPricee: '189,00 zł',
          discount: '-15%',
          subtitle: '5l'
        }}
      />
    );
    expect(withData);
  });
  it('With all data without discount', () => {
    const withoutDiscount = shallow(
      <ProductItem
        data={{
          name: 'Havoline Magnetic',
          subname: 'EVO 15W-40',
          title: 'Olej przekładniowy',
          startingPrice: '189,00 zł',
          discount: null,
          salePrice: '149,00 zł',
          subtitle: '5l'
        }}
      />
    );
    expect(withoutDiscount);
  });
  it('With all data without name', () => {
    const withoutName = shallow(
      <ProductItem
        data={{
          name: null,
          subname: 'EVO 15W-40',
          title: 'Olej przekładniowy',
          startingPrice: '189,00 zł',
          discount: '-15%',
          salePrice: '149,00 zł',
          subtitle: '5l'
        }}
      />
    );
    expect(withoutName);
  });
  it('Mock with all data', () => {
    const mockTest = mount(
      <MockedProvider mocks={Mocks} addTypename={false}>
        <ProductItem />
      </MockedProvider>
    );
    expect(mockTest);
  });
});

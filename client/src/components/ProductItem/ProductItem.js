import React, { useState, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import { Query, Mutation } from 'react-apollo';
import {
  GET_PRODUCT,
  EDIT_PRODUCT_ITEM,
  EDIT_FOLLOW_ITEM
} from '../../pages/HomePage/HomePage.query';

import {
  Box,
  ButtonMore,
  ButtonAddProduct,
  ButtonDeleteProduct,
  ButtonFollowProduct,
  ButtonUnfollowProduct,
  SalePrice,
  StartingPrice,
  Name,
  Subname,
  Title,
  Subtitle,
  Discount,
  ButtonCloseMoreBox,
  MoreBox
} from './ProductItem.style';

function ProductItem({ data }) {
  const [isDiscount, setIsDiscount] = useState(false);
  const [isMoreButton, setIsMoreButton] = useState(false);
  const [isFollowProduct, setIsFollowProduct] = useState(false);
  const [isAddProduct, setIsAddProduct] = useState(false);

  useEffect(() => {
    if (data)
      setIsDiscount(
        data.discount && data.startingPrice !== null ? true : false
      );
  }, [data]);

  return (
    <>
      <Query query={GET_PRODUCT}>
        {({ loading, error, data }) => {
          console.log(loading, error, data);
          const { product } = data;
          if (loading) return <Box>Loading...</Box>;
          if (error) return <Box>Error! {error.message}</Box>;
          return (
            <Box>
              {isMoreButton ? (
                <>
                  <ButtonCloseMoreBox
                    href="javascript:void(0)"
                    onClick={() => {
                      setIsMoreButton(false);
                    }}
                  >
                    <FormattedMessage
                      id={'LANG_PRODUCT_ITEM.BUTTON_CLOSE_BOX'}
                      defaultMessage={'Close'}
                    />
                  </ButtonCloseMoreBox>
                  <MoreBox>{product.more}</MoreBox>
                </>
              ) : (
                <>
                  {isDiscount ? (
                    <>
                      <StartingPrice>{product.startingPrice}</StartingPrice>
                      <Discount>{product.discount}</Discount>
                    </>
                  ) : (
                    <div style={{ padding: '1.2em' }}></div>
                  )}
                  <SalePrice>{product.salePrice}</SalePrice>
                  <Name>{product.name}</Name>
                  <Subname>{product.subname}</Subname>
                  <Title>{product.title}</Title>
                  <Subtitle>{product.subtitle}</Subtitle>
                  <ButtonMore
                    href="javascript:void(0)"
                    onClick={() => {
                      setIsMoreButton(true);
                    }}
                  >
                    <FormattedMessage
                      id={'LANG_PRODUCT_ITEM.BUTTON_MORE'}
                      defaultMessage={'More'}
                    />
                  </ButtonMore>
                  <Mutation mutation={EDIT_PRODUCT_ITEM}>
                    {(editAddProductItem, { data }) =>
                      isAddProduct ? (
                        <ButtonDeleteProduct
                          href="javascript=void(0)"
                          onClick={() => {
                            setIsAddProduct(false);
                            alert('Usunięto produkt z koszyka');
                            editAddProductItem({
                              variables: { isAddProduct: false }
                            });
                          }}
                        >
                          <FormattedMessage
                            id={'LANG_PRODUCT_ITEM.BUTTON_DELETE_PRODUCT'}
                            defaultMessage={'Delete from cart'}
                          />
                        </ButtonDeleteProduct>
                      ) : (
                        <ButtonAddProduct
                          href="javascript:void(0)"
                          onClick={() => {
                            setIsAddProduct(true);
                            alert('Dodano produkt do koszyka.');
                            editAddProductItem({
                              variables: { isAddProduct: true }
                            });
                          }}
                        >
                          <FormattedMessage
                            id={'LANG_PRODUCT_ITEM.BUTTON_ADD_PRODUCT'}
                            defaultMessage={'Add to cart'}
                          />
                        </ButtonAddProduct>
                      )
                    }
                  </Mutation>
                  <Mutation mutation={EDIT_FOLLOW_ITEM}>
                    {(editFollowProductItem, { data }) =>
                      isFollowProduct ? (
                        <ButtonUnfollowProduct
                          href="javascript:void(0)"
                          onClick={() => {
                            setIsFollowProduct(false);
                            editFollowProductItem({
                              variables: { isFollowProduct: false }
                            });
                          }}
                        >
                          <FormattedMessage
                            id={'LANG_PRODUCT_ITEM.BUTTON_UNFOLLOW_PRODUCT'}
                            defaultMessage={'Following product'}
                          />
                        </ButtonUnfollowProduct>
                      ) : (
                        <ButtonFollowProduct
                          href="javascript:void(0)"
                          onClick={() => {
                            setIsFollowProduct(true);
                            editFollowProductItem({
                              variables: { isFollowProduct: true }
                            });
                          }}
                        >
                          <FormattedMessage
                            id={'LANG_PRODUCT_ITEM.BUTTON_FOLLOW_PRODUCT'}
                            defaultMessage={'Follow product'}
                          />
                        </ButtonFollowProduct>
                      )
                    }
                  </Mutation>
                </>
              )}
            </Box>
          );
        }}
      </Query>
    </>
  );
}

export default ProductItem;

import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import { storiesOf, addDecorator } from '@storybook/react';
import { IntlProviderWrapper } from '../../IntlContext';
import CssBaseline from '@material-ui/core/CssBaseline';

import ProductItem from './ProductItem';
import Mocks from '../../Mocks';

addDecorator(story => (
  <CssBaseline>
    <MockedProvider mocks={Mocks} addTypename={false}>
      <IntlProviderWrapper>{story()}</IntlProviderWrapper>
    </MockedProvider>
  </CssBaseline>
));

storiesOf('ProductItem', module)
  .add('With all data', () => (
    <ProductItem
      data={{
        name: 'Havoline Magnetic',
        subname: 'EVO 15W-40',
        title: 'Olej przekładniowy',
        salePrice: '149,00 zł',
        startingPrice: '189,00 zł',
        discount: '-15%',
        subtitle: '5l'
      }}
    />
  ))
  .add('With all data without discount', () => (
    <ProductItem
      data={{
        name: 'Havoline Magnetic',
        subname: 'EVO 15W-40',
        title: 'Olej przekładniowy',
        startingPrice: '189,00 zł',
        discount: null,
        salePrice: '149,00 zł',
        subtitle: '5l'
      }}
    />
  ))
  .add('With all data without name', () => (
    <ProductItem
      data={{
        name: null,
        subname: 'EVO 15W-40',
        title: 'Olej przekładniowy',
        startingPrice: '189,00 zł',
        discount: '-15%',
        salePrice: '149,00 zł',
        subtitle: '5l'
      }}
    />
  ))
  .add('Mock with all data without discount', () => (
    <MockedProvider mocks={Mocks} addTypename={false}>
      <ProductItem />
    </MockedProvider>
  ));

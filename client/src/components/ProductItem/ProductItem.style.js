import styled from 'styled-components';
import { Colors } from 'assets/Variables.style';

export const Box = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: 190px 5px;
  grid-template-rows: 55px 30px 15px 23px 15px 23px 40px 40px;
  height: 250px;
  width: 200px;
  padding: 5px;
  border-radius: 3px;
  border: 1px solid ${Colors.alto};
  background-color: ${Colors.mineShaft};
  font-family: Arial, Helvetica, sans-serif;
  font-size: 15px;
  font-weight: 400;
  line-height: 1.43;
`;
export const StartingPrice = styled.div`
  grid-column: 1/2;
  grid-row: 1/9;
  padding-left: 20px;
  color: ${Colors.gray};
  font-size: 14pt;
  text-decoration: line-through;
`;
export const Discount = styled.span`
  position: relative;
  grid-column: 2/2;
  grid-row: 1/9;
  left: -55px;
  top: 1px;
  height: 25px;
  width: 45px;
  padding-right: 5px;
  padding-left: 5px;
  border-radius: 3px 3px 3px 3px;
  color: ${Colors.white};
  background-color: ${Colors.red};
  font-weight: bold;
`;
export const SalePrice = styled.div`
  position: relative;
  grid-column: 1/2;
  grid-row: 2/9;
  top: -24px;
  padding-left: 20px;
  color: ${Colors.white};
  font-size: 18pt;
  font-weight: bold;
`;
export const Name = styled.div`
  position: relative;
  display: block;
  grid-column: 1/2;
  grid-row: 3/9;
  top: -24px;
  padding-left: 20px;
  padding-top: 2px;
  color: ${Colors.white};
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  font-size: 10pt;
  font-weight: bold;
`;
export const Subname = styled.div`
  position: relative;
  display: block;
  grid-column: 1/2;
  grid-row: 4/9;
  top: -24px;
  padding-left: 20px;
  padding-top: 2px;
  color: ${Colors.white};
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  font-size: 10pt;
  font-weight: bold;
`;
export const Title = styled.div`
  position: relative;
  display: block;
  grid-column: 1/2;
  grid-row: 5/9;
  top: -24px;
  padding-left: 20px;
  color: ${Colors.gray};
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  font-size: 8pt;
`;
export const Subtitle = styled.div`
  position: relative;
  display: block;
  grid-column: 1/2;
  grid-row: 6/9;
  top: -24px;
  padding-left: 20px;
  color: ${Colors.gray};
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  font-size: 8pt;
`;
export const ButtonMore = styled.button`
  position: relative;
  display: inline-block;
  grid-column: 1;
  grid-row: 7/9;
  top: -24px;
  height: 30px;
  width: 100%;
  padding: 5px 10px 5px 10px;
  margin-bottom: 5px;
  margin-top: 5px;
  border: 0;
  border-radius: 3px 3px 3px 3px;
  color: ${Colors.black};
  background-color: ${Colors.alto};
  text-decoration: none;
  font-weight: bold;
  font-size: 11px;
  text-align: center;
`;

export const MoreBox = styled.div`
  height: 180px;
  margin: 8px;
  padding: 5px;
  color: ${Colors.silverChalice};
  overflow: auto;
  font-size: 14px;
`;

export const ButtonCloseMoreBox = styled.button`
  position: absolute;
  bottom: 20px;
  display: inline-block;
  top: 210px;
  height: 30px;
  width: 190px;
  padding: 5px 10px 5px 10px;
  margin-bottom: 5px;
  margin-top: 5px;
  margin-left: 4px;
  border: 0;
  border-radius: 3px 3px 3px 3px;
  color: ${Colors.black};
  background-color: ${Colors.alto};
  text-decoration: none;
  font-weight: bold;
  font-size: 11px;
  text-align: center;
`;

export const ButtonAddProduct = styled.button`
  position: relative;
  display: inline-block;
  grid-column: 1;
  grid-row: 8/9;
  top: -24px;
  height: 35px;
  width: 100%;
  padding: 5px 10px 5px 10px;
  border: 0;
  border-radius: 3px 3px 3px 3px;
  margin-bottom: 5px;
  color: ${Colors.white};
  background-color: ${Colors.funGreen};
  text-decoration: none;
  font-weight: bold;
  font-size: 13px;
`;

export const ButtonDeleteProduct = styled.button`
  position: relative;
  display: inline-block;
  grid-column: 1;
  grid-row: 8/9;
  top: -24px;
  height: 35px;
  width: 100%;
  padding: 5px 10px 5px 10px;
  border: 0;
  border-radius: 3px 3px 3px 3px;
  margin-bottom: 5px;
  color: ${Colors.white};
  background-color: ${Colors.red};
  text-decoration: none;
  font-weight: bold;
  font-size: 13px;
`;

export const ButtonFollowProduct = styled.button`
  position: relative;
  display: inline-block;
  grid-column: 1;
  grid-row: 9/9;
  top: -24px;
  height: 7px;
  width: 100%;
  padding: 5px 10px 15px 10px;
  border-radius: 3px 3px 3px 3px;
  border-width: 1px;
  color: ${Colors.alto};
  background-color: ${Colors.mineShaft};
  text-decoration: none;
  font-size: 10px;
`;

export const ButtonUnfollowProduct = styled.button`
  position: relative;
  display: inline-block;
  grid-column: 1;
  grid-row: 9/9;
  top: -24px;
  height: 7px;
  width: 100%;
  padding: 5px 10px 15px 10px;
  border-radius: 3px 3px 3px 3px;
  border-width: 1px;
  color: ${Colors.alto};
  background-color: ${Colors.mineShaft};
  text-decoration: none;
  font-size: 10px;
`;

export const ListProduct = styled.div`
  display: grid;
  grid-template-columns: 200px 200px 200px 200px 200px;
  grid-gap: 10px;
`;

export const Array = styled.div`
  position: relative;
  width: 1020px;
  height: 40px;
  background: ${Colors.treePoppy};
  color: ${Colors.red};
  font-size: 30px;
  font-weight: bold;
  text-align: center;
  :after {
    content: '';
    position: absolute;
    left: 0;
    bottom: 0;
    width: 0;
    height: 0;
    border-left: 20px solid white;
    border-top: 20px solid transparent;
    border-bottom: 20px solid transparent;
    background: ${Colors.red};
  }
  :before {
    content: '';
    position: absolute;
    right: -20px;
    bottom: 0;
    width: 0;
    height: 0;
    border-left: 20px solid red;
    border-top: 20px solid transparent;
    border-bottom: 20px solid transparent;
  }
`;

export const SingleItem = styled.div`
  position: relative;
  width: 1020px;
  height: 40px;
  background: ${Colors.treePoppy};
  color: ${Colors.red};
  font-size: 30px;
  font-weight: bold;
  text-align: center;
  :after {
    content: '';
    position: absolute;
    left: 0;
    bottom: 0;
    width: 0;
    height: 0;
    border-left: 20px solid white;
    border-top: 20px solid transparent;
    border-bottom: 20px solid transparent;
    background: ${Colors.red};
  }
  :before {
    content: '';
    position: absolute;
    right: -20px;
    bottom: 0;
    width: 0;
    height: 0;
    border-left: 20px solid red;
    border-top: 20px solid transparent;
    border-bottom: 20px solid transparent;
  }
`;

export const Alerts = styled.div`
  position: relative;
  width: 1020px;
  height: 40px;
  background: ${Colors.treePoppy};
  color: ${Colors.red};
  font-size: 30px;
  font-weight: bold;
  text-align: center;
  :after {
    content: '';
    position: absolute;
    left: 0;
    bottom: 0;
    width: 0;
    height: 0;
    border-left: 20px solid white;
    border-top: 20px solid transparent;
    border-bottom: 20px solid transparent;
    background: ${Colors.red};
  }
  :before {
    content: '';
    position: absolute;
    right: -20px;
    bottom: 0;
    width: 0;
    height: 0;
    border-left: 20px solid red;
    border-top: 20px solid transparent;
    border-bottom: 20px solid transparent;
  }
`;

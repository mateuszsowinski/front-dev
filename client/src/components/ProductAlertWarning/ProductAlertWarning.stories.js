import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import { storiesOf, addDecorator } from '@storybook/react';
import { IntlProviderWrapper } from '../../IntlContext';
import CssBaseline from '@material-ui/core/CssBaseline';

import ProductAlertWarning from './ProductAlertWarning';
import Mocks from '../../Mocks';

addDecorator(story => (
  <CssBaseline>
    <MockedProvider mocks={Mocks} addTypename={false}>
      <IntlProviderWrapper>{story()}</IntlProviderWrapper>
    </MockedProvider>
  </CssBaseline>
));

storiesOf('Product Alert Warning', module)
  .add('Init', () => <ProductAlertWarning data={{}} />)
  .add('With all data', () => (
    <ProductAlertWarning
      data={{
        title: 'Zanim kupisz!',
        subtitle: 'Sprawdź, czy produkt pasuje do Twojego samochodu',
        description:
          'Nasz sklep umożliwia ściśle dopasowanie produktów do wybranego typu pojazdu. Zanim kupisz produkt w sklepie upewnij się czy pasuje do Twojego auta. Wystarczy...'
      }}
    />
  ))
  .add('With all data without title', () => (
    <ProductAlertWarning
      data={{
        title: null,
        subtitle: 'Sprawdź, czy produkt pasuje do Twojego samochodu',
        description:
          'Nasz sklep umożliwia ściśle dopasowanie produktów do wybranego typu pojazdu. Zanim kupisz produkt w sklepie upewnij się czy pasuje do Twojego auta. Wystarczy...'
      }}
    />
  ))
  .add('With all data without subtitle', () => (
    <ProductAlertWarning
      data={{
        title: 'Zanim kupisz!',
        subtitle: null,
        description:
          'Nasz sklep umożliwia ściśle dopasowanie produktów do wybranego typu pojazdu. Zanim kupisz produkt w sklepie upewnij się czy pasuje do Twojego auta. Wystarczy...'
      }}
    />
  ))
  .add('With all data without description', () => (
    <ProductAlertWarning
      data={{
        title: 'Zanim kupisz!',
        subtitle: 'Sprawdź, czy produkt pasuje do Twojego samochodu',
        description: null
      }}
    />
  ))
  .add('Mock with all data', () => (
    <MockedProvider mocks={Mocks} addTypename={false}>
      <ProductAlertWarning />
    </MockedProvider>
  ));

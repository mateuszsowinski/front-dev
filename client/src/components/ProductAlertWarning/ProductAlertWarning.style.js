import styled from 'styled-components';
import { Colors } from 'assets/Variables.style';

export const Box = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: 40px 300px;
  grid-template-rows: 17px 30px 40px 40px;
  height: 174px;
  width: 350px;
  padding: 5px;
  margin-bottom: 20px;
  background-color: ${Colors.mercuryTwo};
  padding: 10px;
`;

export const Title = styled.div`
  position: relative;
  grid-column: 2/2;
  grid-row: 1/4;
  font-size: 14px;
  font-weight: bold;
`;

export const Subtitle = styled.div`
  position: relative;
  grid-column: 2/2;
  grid-row: 2/4;
  color: ${Colors.scorpion};
  font-size: 13px;
  font-weight: bold;
`;

export const ButtonSelect = styled.button`
  position: relative;
  display: inline-block;
  grid-row: 3/4;
  height: 27px;
  width: 180px;
  padding: 5px 5px 5px 5px;
  margin-top: 15px;
  margin-left: 70px;
  border: 0;
  border-radius: 3px 3px 3px 3px;
  color: ${Colors.white};
  background-color: ${Colors.scienceBlue};
  text-decoration: none;
  font-size: 13px;
  font-weight: bold;
  text-align: center;
`;

export const Description = styled.div`
  position: relative;
  grid-row: 4/4;
  width: 325px;
  margin-left: 5px;
  margin-top: 15px;
  font-size: 11px;
`;

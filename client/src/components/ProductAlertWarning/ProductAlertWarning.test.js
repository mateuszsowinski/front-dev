import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';

import { shallow, mount } from 'enzyme';
import ProductAlertWarning from './ProductAlertWarning';

import Mocks from '../../Mocks';

describe('Product Alert Warning', () => {
  it('Init', () => {
    const init = shallow(<ProductAlertWarning />);
    expect(init);
  });
  it('With all data', () => {
    const withData = shallow(
      <ProductAlertWarning
        data={{
          title: 'Zanim kupisz!',
          subtitle: 'Sprawdź, czy produkt pasuje do Twojego samochodu',
          description:
            'Nasz sklep umożliwia ściśle dopasowanie produktów do wybranego typu pojazdu. Zanim kupisz produkt w sklepie upewnij się czy pasuje do Twojego auta. Wystarczy...'
        }}
      />
    );
    expect(withData);
  });
  it('With all data without title', () => {
    const withoutProductInformation = shallow(
      <ProductAlertWarning
        data={{
          title: null,
          subtitle: 'Sprawdź, czy produkt pasuje do Twojego samochodu',
          description:
            'Nasz sklep umożliwia ściśle dopasowanie produktów do wybranego typu pojazdu. Zanim kupisz produkt w sklepie upewnij się czy pasuje do Twojego auta. Wystarczy...'
        }}
      />
    );
    expect(withoutProductInformation);
  });
  it('With all data without subtitle', () => {
    const withoutProductInformation = shallow(
      <ProductAlertWarning
        data={{
          title: 'Zanim kupisz!',
          subtitle: null,
          description:
            'Nasz sklep umożliwia ściśle dopasowanie produktów do wybranego typu pojazdu. Zanim kupisz produkt w sklepie upewnij się czy pasuje do Twojego auta. Wystarczy...'
        }}
      />
    );
    expect(withoutProductInformation);
  });
  it('With all data without description', () => {
    const withoutDescription = shallow(
      <ProductAlertWarning
        data={{
          title: 'Zanim kupisz!',
          subtitle: 'Sprawdź, czy produkt pasuje do Twojego samochodu',
          description: null
        }}
      />
    );
    expect(withoutDescription);
  });
  it('Mock with all data', () => {
    const mockTest = mount(
      <MockedProvider mocks={Mocks} addTypename={false}>
        <ProductAlertWarning />
      </MockedProvider>
    );
    expect(mockTest);
  });
});

import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Query } from 'react-apollo';
import { GET_PRODUCT_ALERT_WARNING } from '../../pages/HomePage/HomePage.query';
import {
  Box,
  Title,
  Subtitle,
  ButtonSelect,
  Description
} from './ProductAlertWarning.style';

import { FaExclamation, FaTimes } from 'react-icons/fa';

import { IconContext } from 'react-icons';

function ProductAlertWarning({ data }) {
  return (
    <>
      <Query query={GET_PRODUCT_ALERT_WARNING}>
        {({ loading, error, data }) => {
          console.log(loading, error, data);
          const { productAlertWarning } = data;
          if (loading) return <Box>Loading...</Box>;
          if (error) return <Box>Error! {error.message}</Box>;
          return (
            <Box>
              <IconContext.Provider
                value={{
                  style: {
                    height: '30px',
                    width: '30px',
                    color: 'white',
                    backgroundColor: 'black',
                    marginTop: '5px',
                    borderRadius: '3px 3px 3px 3px',
                    padding: '7px'
                  }
                }}
              >
                <>
                  <FaExclamation />
                </>
              </IconContext.Provider>
              <IconContext.Provider
                value={{
                  style: {
                    margin: '-23px 0px 0px 320px',
                    height: '15px',
                    width: '15px',
                    fontWeight: 'bold'
                  }
                }}
              >
                <>
                  <FaTimes />
                </>
              </IconContext.Provider>
              <Title>{productAlertWarning.title}</Title>
              <Subtitle>{productAlertWarning.subtitle}</Subtitle>
              <ButtonSelect>
                <FormattedMessage
                  id={'LANG_PRODUCT_ALERT_WARNING.BUTTON_SELECT'}
                  defaultMessage={'Select a car'}
                />
              </ButtonSelect>
              <Description>{productAlertWarning.description}</Description>
            </Box>
          );
        }}
      </Query>
    </>
  );
}

export default ProductAlertWarning;
